﻿using NUnit.Framework;
using PankkiTesteri;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PankkiTesteriTests
{
    [TestFixture]
    public class PankkitiliTest
    {
        /*
         *PankktiliSovellus sisältää:
          *
          *Luokka, jonka nimi on Pankktili
          * Pankkitilissä on kolme toimintoa
          * 
          * -Tallettaa rahaa
          * - Nostaa rahaa
          * -- Tarkistetaan ettei tili mene miinukselle
          * -Siirtää rahaa tililtä toiselle
          * -- Tarkistetaan ettei tili mene miinukselle
          * 
          */
        public Pankkitili tili1= null;
        //OneTimeSetUp
        //OneTimeTearDown
        //TearDown
          [SetUp]
          public void TestienAlustaja()
        {
            this.tili1 = new Pankkitili(100);
        }
        
        [Test]
        public void LuoPankkitili()
        {
           
         
            //Testataan olion luokan tyyppi
            Assert.IsInstanceOf<Pankkitili>(tili1);

        }

        [Test]
        public void AsetaPankkitililleAlkusaldo()
        {
          

            //Testataan yhtäsuuruutta
            Assert.That(100, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void TallettaaRahaaPankkitilille()
        {

            //Tallettaa rahaa tilille

            tili1.Talleta(250);
            //Testataan yhtäsuuruutta
            Assert.That(350, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void NostaPankkitililtäRahaa()
        {
            tili1.NostaRahaa(75);
            Assert.That(25, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void NostonJalkeenPankkitiliEiVoiOllaMiinuksella()
        {

            //Testataan antaako ohjelma halutun virhepyynnön
            Assert.Throws<ArgumentException>(() =>tili1.NostaRahaa(175));
            
            //Vaikka virhe sattuu niin rahoja ei menetä
            //pitäisi olla loppusaldo kanssa sama.
            Assert.That(100, Is.EqualTo(tili1.Saldo));
        }
         
    }
}
